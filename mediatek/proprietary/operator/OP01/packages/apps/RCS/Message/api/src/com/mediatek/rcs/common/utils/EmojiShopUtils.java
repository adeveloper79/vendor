package com.mediatek.rcs.common.utils;

import android.util.Log;

import java.util.HashMap;

public class EmojiShopUtils {

    private static final String TAG = "EmojiShopUtils";

    // EmData in user folder, <emId, emPath>
    private static final HashMap<String, String> mUserEmPathMap = new HashMap<String, String>();

    // EmData in download folder, <emId, emPath>
    private static final HashMap<String, String> mDownloadEmMap = new HashMap<String, String>();

    public static void clearUserEmPaths() {
        Log.d(TAG, "clearUserEmPaths");
        mUserEmPathMap.clear();
    }

    public static boolean addUserEmPath(String emId, String path) {
        if (!mUserEmPathMap.containsKey(emId)) {
            mUserEmPathMap.put(emId, path);
            return true;
        }
        return false;
    }

    public static void clearDownloadEmPaths() {
        Log.d(TAG, "clearUserEmPaths");
        mDownloadEmMap.clear();
    }

    public static boolean addDownloadEmPath(String emId, String path) {
        if (!mDownloadEmMap.containsKey(emId)) {
            mDownloadEmMap.put(emId, path);
            return true;
        }
        return false;
    }

    public static boolean containsEmId(String emId) {
        return mUserEmPathMap.containsKey(emId) ||
               mDownloadEmMap.containsKey(emId);
    }

    public static String getEmPathById(String emId) {
        if (mDownloadEmMap.containsKey(emId)) {
            return mDownloadEmMap.get(emId);
        }
        if (mUserEmPathMap.containsKey(emId)) {
            return mUserEmPathMap.get(emId);
        }
        return null;
    }
}
