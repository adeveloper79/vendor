//MTK_SWIP_PROJECT_START
#ifndef __MTK_LCE_H__
#define __MTK_LCE_H__

#define LCE_ALGO_CHIP_VERSION       (6797)
#define LCE_ALGO_MAIN_VERSION       (1)
#define LCE_ALGO_SUB_VERSION        (1)
#define LCE_ALGO_SYSTEM_VERSION     (2)

#include "MTKLceType.h"
#include "MTKLceErrCode.h"


/*****************************************************************************
    Feature Control Enum and Structure
******************************************************************************/

typedef enum
{
	MTKLCE_FEATURE_BEGIN,				// minimum of feature id
	MTKLCE_FEATURE_SET_ENV_INFO,		// feature id to setup environment information
    MTKLCE_FEATURE_SET_PROC_INFO,		// feature id to setup processing information
    MTKLCE_FEATURE_GET_RESULT,			// feature id to get result
    MTKLCE_FEATURE_GET_EXIF,				// feature id to get EXIF
	MTKLCE_FEATURE_CONFIG_SMOOTH,
	MTKLCE_FEATURE_CONFIG_FLARE,
	MTKLCE_FEATURE_CONFIG_ENHANCE_FACTOR,
	MTKLCE_FEATURE_CONFIG_MODE,
    MTKLCE_FEATURE_MAX					// maximum of feature id
}	MTKLCE_FEATURE_ENUM;

typedef enum
{
    eLCESensorDev_None         = 0x00,
    eLCESensorDev_Main         = 0x01,
    eLCESensorDev_Sub          = 0x02,
	eLCESensorDev_MainSecond   = 0x04,
    eLCESensorDev_Main3D       = 0x05,
	eLCESensorDev_SubSecond    = 0x08
}   eLCESensorDev_T;


#define LCE_AE_RGBSUM_HIST_TOTAL_COUNT          ((unsigned long long)24000000)     // 4208*3120*3/8
#define LCE_AE_HISTOGRAM_BIN                    (128)
#define LCE_LV_INDEX_UNIT                       (10)        // 1 LV
#define LCE_LV_INDEX_MIN                        (0)         // 0 LV
#define LCE_LV_INDEX_MAX                        (18)        // LV 18
#define LCE_LV_INDEX_NUM                        ((LCE_LV_INDEX_MAX - LCE_LV_INDEX_MIN) + 1)
#define LCE_CONTRAST_INDEX_UNIT                 (10)
#define LCE_CONTRAST_INDEX_MIN                  (0)
#define LCE_CONTRAST_INDEX_MAX                  (10)
#define LCE_CONTRAST_INDEX_NUM                  ((LCE_CONTRAST_INDEX_MAX - LCE_CONTRAST_INDEX_MIN) + 1)



typedef struct {
    MINT32 i4LCEBa;
    MINT32 i4LCEPa[LCE_CONTRAST_INDEX_NUM][LCE_LV_INDEX_NUM];
    MINT32 i4LCEPb[LCE_CONTRAST_INDEX_NUM][LCE_LV_INDEX_NUM];
} MTK_LCE_TUNING_LUT_T;

typedef struct {
    MINT32 i4Enable;
    MINT32 i4WaitAEStable;
    MINT32 i4Speed;     // 0 ~ 10
} MTK_LCE_TUNING_SMOOTH_T;

typedef struct {
    MINT32 i4Enable;
} MTK_LCE_TUNING_FLARE_T;



typedef enum {
    eLCE_NORMAL_MODE = 0,
    eLCE_HDR_MODE,
    eLCE_MODE_NUM
} LCE_MODE_T;

typedef enum {
    // Camera1.0/Camera3.0
    eLCEProfile_Preview = 0,          // Preview
    eLCEProfile_Video,                // Video
    eLCEProfile_Capture,              // Capture
    eLCEProfile_ZSD_Capture,          // ZSD Capture
    eLCEProfile_VSS_Capture,          // VSS Capture
    // Camera1.0
    eLCEProfile_PureRAW_Capture,      // Pure RAW Capture
    // N3D
    eLCEProfile_N3D_Preview,          // N3D Preview
    eLCEProfile_N3D_Video,            // N3D Video
    eLCEProfile_N3D_Capture,          // N3D Capture
    eLCEProfile_N3D_Denoise,
    // MFB
    eLCEProfile_MFB_Capture_EE_Off,   // MFB capture: EE off
    eLCEProfile_MFB_Capture_EE_Off_SWNR, // MFB capture with SW NR: EE off
    eLCEProfile_MFB_Blending_All_Off, // MFB blending: all off
    eLCEProfile_MFB_Blending_All_Off_SWNR, // MFB blending with SW NR: all off
    eLCEProfile_MFB_PostProc_EE_Off,  // MFB post process: capture + EE off
    eLCEProfile_MFB_PostProc_ANR_EE,  // MFB post process: capture + ANR + EE
    eLCEProfile_MFB_PostProc_ANR_EE_SWNR,  // MFB post process with SW NR: capture + ANR + EE
    eLCEProfile_MFB_PostProc_Mixing,  // MFB post process: mixing + all off
    eLCEProfile_MFB_PostProc_Mixing_SWNR,  // MFB post process with SW NR: mixing + all off
    // vFB
    eLCEProfile_VFB_PostProc,         // VFB post process: all off + ANR + CCR + PCA
    // iHDR
    eLCEProfile_IHDR_Preview,         // IHDR preview
    eLCEProfile_IHDR_Video,           // IHDR video
    // zHDR
    eLCEProfile_ZHDR_Preview,         // ZHDR preview
    eLCEProfile_ZHDR_Video,           // ZHDR video
    eLCEProfile_ZHDR_Capture, //
    // Multi-pass ANR
    eLCEProfile_Capture_MultiPass_ANR_1,     // Capture multi pass ANR 1
    eLCEProfile_Capture_MultiPass_ANR_2,     // Capture multi pass ANR 2
    eLCEProfile_VSS_Capture_MultiPass_ANR_1, // VSS capture multi Pass ANR 1
    eLCEProfile_VSS_Capture_MultiPass_ANR_2, // VSS capture multi Pass ANR 2
    eLCEProfile_MFB_MultiPass_ANR_1, // MFB multi Pass ANR 1
    eLCEProfile_MFB_MultiPass_ANR_2, // MFB multi Pass ANR 2
    eLCEProfile_Capture_SWNR, // Capture with SW NR
    eLCEProfile_VSS_Capture_SWNR, // VSS capture with SW NR
    eLCEProfile_PureRAW_Capture_SWNR, // Pure RAW capture with SW NR
    // mHDR
    eLCEProfile_MHDR_Preview,         // MHDR preview
    eLCEProfile_MHDR_Video,           // MHDR video
    eLCEProfile_MHDR_Capture, //

    // VSS_MFB (ZSD+MFLL)
    eLCEProfile_VSS_MFB_Capture_EE_Off,   // MFB capture: EE off
    eLCEProfile_VSS_MFB_Capture_EE_Off_SWNR, // MFB capture with SW NR: EE off
    eLCEProfile_VSS_MFB_Blending_All_Off, // MFB blending: all off
    eLCEProfile_VSS_MFB_Blending_All_Off_SWNR, // MFB blending with SW NR: all
    eLCEProfile_VSS_MFB_PostProc_EE_Off,  // MFB post process: capture + EE off
    eLCEProfile_VSS_MFB_PostProc_ANR_EE,  // MFB post process: capture + ANR +
    eLCEProfile_VSS_MFB_PostProc_ANR_EE_SWNR,  // MFB post process with SW NR:
    eLCEProfile_VSS_MFB_PostProc_Mixing,  // MFB post process: mixing + all off
    eLCEProfile_VSS_MFB_PostProc_Mixing_SWNR,  // MFB post process with SW NR:
    eLCEProfile_VSS_MFB_MultiPass_ANR_1, // MFB multi Pass ANR 1
    eLCEProfile_VSS_MFB_MultiPass_ANR_2, // MFB multi Pass ANR 2

    eLCEProfile_NUM
} eLCE_PROFILE_T;

typedef struct {
    MINT32 i4LCEBa;
    MINT32 i4LCEPa;
    MINT32 i4LCEPb;
} MTK_FIXED_LCE_TUNING_T;

typedef struct {
    MINT32 i4LCESeg;
    MINT32 i4LCEContrastRatio;
    MTK_LCE_TUNING_LUT_T rLCELUTs;
    MTK_LCE_TUNING_SMOOTH_T rLCESmooth;
    MTK_LCE_TUNING_FLARE_T rLCEFlare;
} MTK_AUTO_LCE_TUNING_T;


typedef struct {
    MINT32 i4AutoLCEEnable;
    MTK_FIXED_LCE_TUNING_T rDefLCEParam;
    MTK_AUTO_LCE_TUNING_T rAutoLCEParam;
} MTK_LCE_TUNING_PARAM_T;

typedef struct {
    MUINT32 u4AETarget;
    MUINT32 u4AECurrentTarget;
    MUINT32 u4Eposuretime;   //!<: Exposure time in ms
    MUINT32 u4AfeGain;           //!<: raw gain
    MUINT32 u4IspGain;           //!<: sensor gain
    MUINT32 u4RealISOValue;
    MINT32  i4LightValue_x10;
    MUINT32 u4AECondition;
    MINT16  i2FlareOffset;
    MINT32  i4GammaIdx;   // next gamma idx
    MINT32  i4LESE_Ratio;    // LE/SE ratio
    MUINT32 u4SWHDR_SE;      //for sw HDR SE ,  -x EV , compare with converge AE
    MUINT32 u4MaxISO;
    MUINT32 u4AEStableCnt;
    MUINT32 u4OrgExposuretime;   //!<: Exposure time in ms
    MUINT32 u4OrgRealISOValue;
    MUINT32 u4Histogrm[LCE_AE_HISTOGRAM_BIN];
    MBOOL bGammaEnable;

    MINT32 i4AEStable;
    MINT32 i4EVRatio;
}LCE_AE_INFO_T;

typedef struct
{
    MINT32 i4AutoLCEEnable;
    MTK_FIXED_LCE_TUNING_T rDefLCEParam;
    MTK_AUTO_LCE_TUNING_T rAutoLCEParam;
} MTK_LCE_ENV_INFO_STRUCT, *P_MTK_LCE_ENV_INFO_STRUCT;

typedef struct
{
    MINT32 i4LceProfile;
    MINT32 i4LceAutoMode;
	MINT32 i4ChipVersion;
    MINT32 i4MainVersion;
    MINT32 i4SubVersion;
    MINT32 i4SystemVersion;
    MINT32 i4LV;
    MINT32 i4EVRatio;
    MINT32 u4HistTotal;
    MINT32 i4ContrastY10;
    MINT32 i4EVContrastY10;
    MINT32 i4SegDiv;
    MINT32 i4ContrastIdx_L;
    MINT32 i4ContrastIdx_H;
    MINT32 i4LVIdx_L;
    MINT32 i4LVIdx_H;
    MINT32 i4PA;
    MINT32 i4PB;
    MINT32 i4BA;
    MINT32 i4SmoothEnable;
    MINT32 i4SmoothSpeed;
    MINT32 i4SmoothWaitAE;
    MINT32 i4FlareEnable;
    MINT32 i4FlareOffset;
    MINT32 i4FixedPA;
    MINT32 i4FixedPB;
    MINT32 i4FixedBA;
} MTK_LCE_EXIF_INFO_STRUCT, *P_MTK_LCE_EXIF_INFO_STRUCT;

typedef struct
{
    eLCE_PROFILE_T eLceProfile;
	LCE_AE_INFO_T rLCEAEInfo;   // Get current AE related information
} MTK_LCE_PROC_INFO_STRUCT, *P_MTK_LCE_PROC_INFO_STRUCT;

typedef struct
{
    MINT32 LCE_A_PA;
    MINT32 LCE_A_PB;
    MINT32 LCE_A_BA;
} MTK_LCE_RESULT_INFO_STRUCT, *P_MTK_LCE_RESULT_INFO_STRUCT;

class MTKLce
{
public:
    static MTKLce* createInstance(eLCESensorDev_T const eSensorDev);
    virtual MVOID   destroyInstance(MTKLce* obj) = 0;

    virtual ~MTKLce(){}

    // Process Control
    virtual MRESULT LceInit(MVOID *InitInData, MVOID *InitOutData) = 0;
    virtual MRESULT LceMain(void) = 0;
	virtual MRESULT LceExit(void) = 0;
	virtual MRESULT LceReset(void) = 0;                 // RESET for each image

	// Feature Control
	virtual MRESULT LceFeatureCtrl(MUINT32 FeatureID, MVOID* pParaIn, MVOID* pParaOut) = 0;
private:

};



#endif  //__MTK_Lce_H__
//MTK_SWIP_PROJECT_END
