/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkCam/Selector"
//
#include "MyUtils.h"
//
#include <utils/RWLock.h>
//
#include <ImageBufferHeap.h>
#include <v3/utils/streaminfo/ImageStreamInfo.h>
#include <v3/stream/IStreamInfo.h>
//
#include <LegacyPipeline/buffer/Selector.h>
//
#include <camera_custom_zsd.h>
//
#include <feature/include/common/vhdr/1.0/vhdr_type.h>
#include "LegacyPipeline/StreamId.h"
#include <mtk_platform_metadata_tag.h>


using namespace android;
using namespace NSCam;
using namespace NSCam::v1;
using namespace NSCam::v1::NSLegacyPipeline;
using namespace NSCam::Utils;

using namespace NSCam::v3::Utils;
/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("[%s] " fmt, __FUNCTION__, ##arg)
//
#define MY_LOGV_IF(cond, ...)       do { if ( (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if ( (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( (cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( (cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( (cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( (cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)

#if 1
#define FUNC_START     MY_LOGD("+")
#define FUNC_END       MY_LOGD("-")
#else
#define FUNC_START
#define FUNC_END
#endif

/******************************************************************************
 *  Metadata Access
 ******************************************************************************/
template <typename T>
inline MBOOL
tryGetMetadata(
    IMetadata const* pMetadata,
    MUINT32 const tag,
    T & rVal
)
{
    if (pMetadata == NULL) {
        CAM_LOGW("pMetadata == NULL");
        return MFALSE;
    }
    //
    IMetadata::IEntry entry = pMetadata->entryFor(tag);
    if(!entry.isEmpty()) {
        rVal = entry.itemAt(0, Type2Type<T>());
        return MTRUE;
    }
    //
    return MFALSE;
}

/******************************************************************************
 *
 ******************************************************************************/
ZsdSelector::
ZsdSelector()
    : mDelayTimeMs(0)
    , mTolerationMs(0)
    , miLogLevel(1)
    , mUsingBufCount(0)
    , mIsBackupQueue(MFALSE)
    , mIsNeedWaitAfDone(MTRUE)
    , mIsAlreadyStartedCapture(MFALSE)
    , mInactiveCount(0)
{
    miLogLevel = property_get_int32("debug.camera.log", 0);
    if ( miLogLevel == 0 ) {
        miLogLevel = property_get_int32("debug.camera.log.selector", 0);
    }
    //
    mDelayTimeMs = get_zsd_cap_stored_delay_time_ms();
    mTolerationMs = get_zsd_cap_stored_delay_toleration_ms();
    //
    MY_LOGD("new ZsdSelector delay(%d) toler(%d)",mDelayTimeMs,mTolerationMs);
}

/******************************************************************************
 *
 ******************************************************************************/
MINT64
ZsdSelector::
getTimeStamp(Vector<MetaItemSet> &rvResult)
{
    MINT64 newTimestamp = 0;
    for(int i=0; i<rvResult.size(); i++)
    {
        IMetadata::IEntry const entry = rvResult[i].meta.entryFor(MTK_SENSOR_TIMESTAMP);
        if(! entry.isEmpty())
        {
            newTimestamp = entry.itemAt(0, Type2Type<MINT64>());
            break;
        }
    }
    return newTimestamp;
}

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
ZsdSelector::
getAfInfo(Vector<MetaItemSet> &rvResult, MUINT8 &afMode, MUINT8 &afState, MUINT8 &lensState)
{
    MBOOL ret = MTRUE;
    afState = 0;
    afMode = 0;
    lensState = 0;
    bool bFind = false;
    //
    bFind = false;
    for(int i=0; i<rvResult.size(); i++)
    {
        IMetadata::IEntry const entry1 = rvResult[i].meta.entryFor(MTK_CONTROL_AF_STATE);
        IMetadata::IEntry const entry2 = rvResult[i].meta.entryFor(MTK_CONTROL_AF_MODE);
        IMetadata::IEntry const entry3 = rvResult[i].meta.entryFor(MTK_LENS_STATE);
        if(! entry1.isEmpty() && ! entry2.isEmpty() && ! entry3.isEmpty())
        {
            afState = entry1.itemAt(0, Type2Type<MUINT8>());
            afMode = entry2.itemAt(0, Type2Type<MUINT8>());
            lensState = entry3.itemAt(0, Type2Type<MUINT8>());
            bFind = true;
            break;
        }
    }
    if(!bFind)
    {
        MY_LOGW("Can't Find MTK_CONTROL_AF_STATE or MTK_CONTROL_AF_MODE or MTK_LENS_STATE");
        ret = MFALSE;
    }
    //
    return ret;
}

/******************************************************************************
 *
 ******************************************************************************/
MVOID
ZsdSelector::
updateQueueBuffer(MINT64 newTimestamp , int maxUsingBufferNum)
{
    if(mResultSetMap.size()==0)
    {
        MY_LOGD_IF( (2<=miLogLevel), "no buffer, immediately return");
        return;
    }
    //
    if( mIsBackupQueue )
    {
        //only reserve one buffer for backup
        int n = mResultSetMap.size();
        if( n > 1 )
        {
            MY_LOGD_IF( (2<=miLogLevel), "BackkupQueue size(%d) > 1, need to remove", n);
            for(int i=0; i<n-1; i++)
            {
                returnBufferWithoutUsingCnt( mResultSetMap.editItemAt(0).heap, mResultSetMap.editItemAt(0).resultMeta );
                mResultSetMap.removeItemsAt(0);
            }
        }
    }
    else
    {
        //update and check all duration
        int n = mResultSetMap.size();
        int stop = -1;
        for( int i = 0; i < n; i++ )
        {
            MINT32 duration = newTimestamp - mResultSetMap.editItemAt(i).timestamp;
            mResultSetMap.editItemAt(i).duration = duration;
            //
            MY_LOGD_IF( (2<=miLogLevel), "que[%d](%p) Timestamp(%lld) duration(%d)",
                i,mResultSetMap.editItemAt(i).heap.get(),
                mResultSetMap.editItemAt(i).timestamp, duration/1000000);
            //
            if( duration >= (mDelayTimeMs + mTolerationMs)*1000000 )
            {
                stop = i;
            }
        }
        //
        MY_LOGD_IF( (2<=miLogLevel), "stop(%d)",stop);
        //
        //remove too old buffer
        for(int i=0; i<=stop; i++)
        {
            returnBufferWithoutUsingCnt( mResultSetMap.editItemAt(0).heap, mResultSetMap.editItemAt(0).resultMeta );
            mResultSetMap.removeItemsAt(0);
        }
        //
        //remove buffer to be under maxUsingBufferNum
        int del_num = (mResultSetMap.size() + mUsingBufCount) - maxUsingBufferNum + 1;
        //
        MY_LOGD_IF( (2<=miLogLevel), "del_num(%d) = (que size(%d) + using(%d)) - max(%d) + 1",
            del_num, mResultSetMap.size(), mUsingBufCount, maxUsingBufferNum);
        //
        if( del_num > 0 )
        {
            for(int i=0; i<del_num; i++)
            {
                returnBufferWithoutUsingCnt( mResultSetMap.editItemAt(0).heap, mResultSetMap.editItemAt(0).resultMeta );
                mResultSetMap.removeItemsAt(0);
            }
        }
    }
}

/******************************************************************************
*
******************************************************************************/
MBOOL
ZsdSelector::
isOkBuffer(MUINT8 afMode, MUINT8 afState, MUINT8 lensState)
{
    if( mIsNeedWaitAfDone )
    {
        if( afMode == MTK_CONTROL_AF_MODE_AUTO ||
            afMode == MTK_CONTROL_AF_MODE_MACRO )
        {
            mInactiveCount = 0;
            // INACTIVE CASE:
            // IN SCAN CASE:
            if( (afState == MTK_CONTROL_AF_STATE_INACTIVE && mIsAlreadyStartedCapture == MFALSE) ||
            afState == MTK_CONTROL_AF_STATE_PASSIVE_SCAN ||
            afState == MTK_CONTROL_AF_STATE_ACTIVE_SCAN  ||
            lensState == MTK_LENS_STATE_MOVING )
        {
                MY_LOGD_IF( (2<=miLogLevel), "afMode(%d) afState(%d) lensState(%d) startCap(%d) Not OK", afMode, afState, lensState, mIsAlreadyStartedCapture );
                return MFALSE;
            }
            //
            // SCAN DONE CASE:
            MY_LOGD_IF( (2<=miLogLevel), "afMode(%d) afState(%d) lensState(%d) startCap(%d) is OK", afMode, afState, lensState, mIsAlreadyStartedCapture );
        }
        else if( afMode == MTK_CONTROL_AF_MODE_CONTINUOUS_VIDEO ||
                 afMode == MTK_CONTROL_AF_MODE_CONTINUOUS_PICTURE )
        {
            // INACTIVE CASE:
#define MAX_WAIT_INACTIVE_NUM (5)
            if( afState == MTK_CONTROL_AF_STATE_INACTIVE )
            {
                mInactiveCount++;
                //
                if( mInactiveCount >= MAX_WAIT_INACTIVE_NUM )
                {
                    MY_LOGW("afMode(%d) AF in INACTIVE state over (%d) frame, count(%d), the buffer wiil be used!", afMode, MAX_WAIT_INACTIVE_NUM, mInactiveCount );
                    return MTRUE;
                }
                else    //MTK_CONTROL_AF_MODE_OFF or MTK_CONTROL_AF_MODE_EDOF
                {
                    MY_LOGD_IF( (2<=miLogLevel), "afMode(%d) afState(%d) is INACTIVE, count(%d)", afMode, afState, mInactiveCount );
            return MFALSE;
        }
    }
    else
    {
                mInactiveCount = 0;
    }
#undef MAX_WAIT_INACTIVE_NUM
            //
            // IN SCAN CASE:
            if( afState == MTK_CONTROL_AF_STATE_PASSIVE_SCAN ||
                afState == MTK_CONTROL_AF_STATE_ACTIVE_SCAN  ||
                lensState == MTK_LENS_STATE_MOVING )
            {
                MY_LOGD_IF( (2<=miLogLevel), "afMode(%d) afState(%d) lensState(%d) Not OK", afMode, afState, lensState );
                return MFALSE;
            }
            //
            // SCAN DONE CASE:
            MY_LOGD_IF( (2<=miLogLevel), "afMode(%d) afState(%d) lensState(%d) is OK", afMode, afState, lensState );
        }
        else
        {
            mInactiveCount = 0;
            MY_LOGD_IF( (2<=miLogLevel), "afState(%d) is OK", afState );
        }
    }
    else
    {
        mInactiveCount = 0;
        MY_LOGD_IF( (2<=miLogLevel), "No need to see afMode(%d) afState(%d), it is OK", afMode, afState );
    }
    //
    return MTRUE;
}

/******************************************************************************
*
******************************************************************************/
MVOID
ZsdSelector::removeExistBackupBuffer()
{
    if( mIsBackupQueue == MTRUE )
    {
        int n = mResultSetMap.size();
        if( n > 0 )
        {
            MY_LOGD_IF( (2<=miLogLevel), "removeExistBackupBuffer size(%d)", n);
            for(int i=0; i<n; i++)
            {
                returnBufferWithoutUsingCnt( mResultSetMap.editItemAt(0).heap, mResultSetMap.editItemAt(0).resultMeta );
                mResultSetMap.removeItemsAt(0);
            }
        }
        mIsBackupQueue = MFALSE;
    }
}

/******************************************************************************
*
******************************************************************************/
MVOID
ZsdSelector::
insertOkBuffer(ResultSet_T &resultSet)
{
    MY_LOGD_IF( (2<=miLogLevel), "insert Ok Buffer buf(%p) timestamp(%lld) duration(%d) afState(%d)",
        resultSet.heap.get(),resultSet.timestamp,resultSet.duration,resultSet.afState);
    //
    mIsBackupQueue = MFALSE;
    mResultSetMap.push_back(resultSet);
}

/******************************************************************************
*
******************************************************************************/
MVOID
ZsdSelector::
insertBackupBuffer(ResultSet_T &resultSet)
{
    MY_LOGD_IF( (2<=miLogLevel), "insert Backup Buffer buf(%p) timestamp(%lld) duration(%d) afState(%d)",
        resultSet.heap.get(),resultSet.timestamp,resultSet.duration,resultSet.afState);
    //
    mIsBackupQueue = MTRUE;
    mResultSetMap.push_back(resultSet);
}

/******************************************************************************
*
******************************************************************************/
android::status_t
ZsdSelector::
returnBufferWithoutUsingCnt(android::sp<IImageBufferHeap> rpHeap,  Vector<MetaItemSet> &resultMeta)
{
    // Decrease LCSO Buf Reference Count
    for(int i=0 ; i < resultMeta.size() ; i++)
    {
        if(resultMeta[i].id == NSCam::eSTREAMID_META_HAL_DYNAMIC_P1)
        {
            IMetadata::Memory lcei_mem;
            if( tryGetMetadata<IMetadata::Memory>(&resultMeta[i].meta, MTK_VHDR_LCEI_DATA, lcei_mem) ) {
                if (lcei_mem.size() == sizeof(LCEI_CONFIG_DATA)){
                    LCEI_CONFIG_DATA* lceiConfig = (LCEI_CONFIG_DATA*)lcei_mem.array();
                    lceiConfig->FnReleaseHandle(lceiConfig->bufID, lceiConfig->halObj);
                    MY_LOGD_IF(miLogLevel, "[LCSO] return Buffer LCEI, addr = %p ID(%d)", lceiConfig->lcsoBuf, lceiConfig->bufID);
                }else{
                    MY_LOGD_IF(miLogLevel, "[LCSO] zsd Select lcei metatdata size != LCEI_CONFIG_DATA");
                }
             }else{
                MY_LOGD_IF(miLogLevel, "[LCSO] no entry in zsd Selec for MTK_VHDR_LCEI_DATA");
            }
        }
    }
    //
    sp<IConsumerPool> pPool = mpPool.promote();
    if ( pPool == 0 ) {
        MY_LOGE("Cannot promote consumer pool to return buffer.");
        return UNKNOWN_ERROR;
    }
    //
    MY_LOGD_IF( (2<=miLogLevel), "return buf:%p", rpHeap.get());
    return pPool->returnBuffer( rpHeap );
}

/******************************************************************************
 *
 ******************************************************************************/
android::status_t
ZsdSelector::
selectResult(
    MINT32                             rRequestNo,
    Vector<MetaItemSet>                rvResult,
    android::sp<IImageBufferHeap>      rpHeap,
    MBOOL                              errorResult
)
{
    Mutex::Autolock _l(mResultSetLock);
    //
    if( errorResult )
    {
        MY_LOGW("don't reserved errorResult(1) buffer(%p) requestNo(%d)",rpHeap.get(), rRequestNo);
        //
        sp<IConsumerPool> pPool = mpPool.promote();
        if ( pPool != 0 ) {
            pPool->returnBuffer( rpHeap );
        }
        else
        {
            MY_LOGE("returnBuffer requestNo(%d) buffer(%p) failed! pool not exist!",rRequestNo, rpHeap.get());
        }
        //
        return UNKNOWN_ERROR;
    }
    // get timestamp
    MINT64 newTimestamp = getTimeStamp(rvResult);
    if( newTimestamp == 0 )
    {
        MY_LOGW("timestamp == 0");
    }
    //
    // get AF Info
    MUINT8 afMode;
    MUINT8 afState;
    MUINT8 lensState;
    if( !getAfInfo(rvResult, afMode, afState, lensState) )
    {
        MY_LOGW("getAfState Fail!");
    }
    //
    //Update all queue buffer status & remove too old buffer
#define MAX_USING_BUFFER (4)
    updateQueueBuffer(newTimestamp, MAX_USING_BUFFER);
#undef MAX_USING_BUFFER
    //
    if( isOkBuffer(afMode, afState, lensState) )
    {
        if( mIsBackupQueue )
        {
            removeExistBackupBuffer();
        }
        //
        ResultSet_T resultSet = {rRequestNo, rpHeap, rvResult, newTimestamp, 0, afState};
        insertOkBuffer(resultSet);
        //
        mCondResultSet.signal();
    }
    else
    {
        if ( mResultSetMap.isEmpty() )
        {
            ResultSet_T resultSet = {rRequestNo, rpHeap, rvResult, newTimestamp, 0, afState};
            insertBackupBuffer(resultSet);
        }
        else if( mIsBackupQueue )
        {
            removeExistBackupBuffer();
            //
            ResultSet_T resultSet = {rRequestNo, rpHeap, rvResult, newTimestamp, 0, afState};
            insertBackupBuffer(resultSet);
        }
        else
        {
            returnBufferWithoutUsingCnt(rpHeap, rvResult);
        }
    }
    //
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
status_t
ZsdSelector::
getResult(
    MINT32&                          rRequestNo,
    Vector<MetaItemSet>&             rvResultMeta,
    android::sp<IImageBufferHeap>&   rpHeap
)
{
    FUNC_START;
    Mutex::Autolock _l(mResultSetLock);
    MY_LOGD("mResultSetMap size(%d), mIsBackupQueue(%d)",mResultSetMap.size(),mIsBackupQueue);
    if( mResultSetMap.isEmpty() ||
        mIsBackupQueue)
    {
#define WAIT_BUFFER_TIMEOUT (3000000000) //ns
        MY_LOGD("Ready to wait condition for %lld ns",WAIT_BUFFER_TIMEOUT);
        MERROR err = mCondResultSet.waitRelative(mResultSetLock, WAIT_BUFFER_TIMEOUT);
#undef WAIT_BUFFER_TIMEOUT
        if ( err!= OK )
        {
            MY_LOGW("Timeout, no OK result can get");
        }
    }
    //
    if( mResultSetMap.isEmpty() )
    {
        MY_LOGE("mResultSetMap is empty!!");
        mIsAlreadyStartedCapture = MFALSE;
        return UNKNOWN_ERROR;
    }
    //
    rRequestNo   = mResultSetMap.editItemAt(0).requestNo;
    rvResultMeta = mResultSetMap.editItemAt(0).resultMeta;
    rpHeap       = mResultSetMap.editItemAt(0).heap;
    //
    MY_LOGD("get result Idx(%d) rNo(%d) buffer(%p) timestamp(%lld) duration(%d) afState(%d)",
        0,rRequestNo,rpHeap.get(),
        mResultSetMap.editItemAt(0).timestamp,
        mResultSetMap.editItemAt(0).duration,
        mResultSetMap.editItemAt(0).afState);
    //
    if( mResultSetMap.editItemAt(0).afState == MTK_CONTROL_AF_STATE_INACTIVE )
    {
        MY_LOGW("the buffer AF state is INACTIVE!");
    }
    //
    mResultSetMap.removeItemsAt(0);
    mUsingBufCount++;
    mIsBackupQueue = MFALSE;
    mIsAlreadyStartedCapture = MFALSE;
    //
    FUNC_END;
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
status_t
ZsdSelector::
returnBuffer(
    android::sp<IImageBufferHeap>   rpHeap
)
{
    Mutex::Autolock _l(mResultSetLock);
    sp<IConsumerPool> pPool = mpPool.promote();
    if ( pPool == 0 ) {
        MY_LOGE("Cannot promote consumer pool to return buffer.");
        return UNKNOWN_ERROR;
    }

    MY_LOGD_IF( 2, "return buf:%p", rpHeap.get());
    mUsingBufCount--;
    return pPool->returnBuffer( rpHeap );
}

/******************************************************************************
 *
 ******************************************************************************/
status_t
ZsdSelector::
flush()
{
    FUNC_START;
    Mutex::Autolock _l(mResultSetLock);

    if ( !mResultSetMap.isEmpty()) {
#if 0
        sp<IConsumerPool> pPool = mpPool.promote();
        if ( pPool == 0 ) {
            MY_LOGE("Cannot promote consumer pool to return buffer.");
            FUNC_END;
            return UNKNOWN_ERROR;
        }
#endif

        for ( size_t i = 0; i < mResultSetMap.size(); ++i ) {
            returnBufferWithoutUsingCnt(mResultSetMap.editItemAt(i).heap, mResultSetMap.editItemAt(i).resultMeta);
        }

        mResultSetMap.clear();
    }

    FUNC_END;
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
void
ZsdSelector::
onLastStrongRef( const void* /*id*/)
{
    FUNC_START;
    flush();
    FUNC_END;
}

/******************************************************************************
 *
 ******************************************************************************/
void
ZsdSelector::
setDelayTime( MINT32 delayTimeMs, MINT32 tolerationTimeMs )
{
    FUNC_START;
    Mutex::Autolock _l(mResultSetLock);
    mDelayTimeMs = delayTimeMs;
    mTolerationMs = tolerationTimeMs;
    MY_LOGD("Set DelayTime (%d) ms Toleration (%d) ms", mDelayTimeMs, mTolerationMs);
    FUNC_END;
}

/******************************************************************************
 *
 ******************************************************************************/
android::status_t
ZsdSelector::
sendCommand( MINT32 cmd,
                 MINT32 arg1,
                 MINT32 arg2,
                 MINT32 arg3,
                 MVOID* arg4 )
{
    bool ret = true;
    //
    MY_LOGD("Cmd:(%d) arg1(%d) arg2(%d) arg3(%d) arg4(%p)",cmd, arg1, arg2, arg3, arg4);
    //
    switch  (cmd)
    {
        case eCmd_setNeedWaitAfDone:
            {
                Mutex::Autolock _l(mResultSetLock);
                mIsNeedWaitAfDone = MTRUE;
            }
            break;
        case eCmd_setNoWaitAfDone:
            {
                Mutex::Autolock _l(mResultSetLock);
                mIsNeedWaitAfDone = MFALSE;
            }
            break;
        case eCmd_setAlreadyStartedCapture:
            {
                Mutex::Autolock _l(mResultSetLock);
                mIsAlreadyStartedCapture = MTRUE;
            }
            break;
        default:
            break;
    }
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
ZsdFlashSelector::
ZsdFlashSelector()
{
    MY_LOGD("new ZsdFlashSelector");
}

/******************************************************************************
 *
 ******************************************************************************/
android::status_t
ZsdFlashSelector::
selectResult(
    MINT32                             rRequestNo,
    Vector<MetaItemSet>                rvResult,
    android::sp<IImageBufferHeap>      rpHeap,
    MBOOL                              errorResult
)
{
    FUNC_START;
    Mutex::Autolock _l(mResultSetLock);

    MY_LOGD("In ZsdFlashSelector selectResult rRequestNo(%d) %p",rRequestNo, rpHeap.get());

    sp<IConsumerPool> pPool = mpPool.promote();
    if ( pPool == 0 ) {
        MY_LOGE("Cannot promote consumer pool to return buffer.");
        return UNKNOWN_ERROR;
    }

    if(mWaitRequestNo == rRequestNo)
    {
        if( mResultHeap != NULL )
        {
            pPool->returnBuffer( mResultHeap );
            returnMeta(rvResult);
        }
        mResultHeap = rpHeap;
        mResultMeta = rvResult;
        MY_LOGD("find data : rRequestNo(%d) rvResult size(%d) resultHeap(%p)",rRequestNo, rvResult.size(), mResultHeap.get());
        mResultQueueCond.broadcast();
    }
    else
    {
        pPool->returnBuffer( rpHeap );
        returnMeta(rvResult);
    }
    FUNC_END;
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
status_t
ZsdFlashSelector::
getResult(
    MINT32&                          rRequestNo,
    Vector<MetaItemSet>&             rvResultMeta,
    android::sp<IImageBufferHeap>&   rpHeap
)
{
    FUNC_START;
    Mutex::Autolock _l(mResultSetLock);

    MY_LOGD("In ZsdFlashSelector getResult waitRequestNo(%d)",mWaitRequestNo);

    nsecs_t timeout = 3000000000LL; //wait for 3 sec
    status_t status = mResultQueueCond.waitRelative(mResultSetLock,timeout);
    if(status != OK)
    {
        MY_LOGE("wait result timeout...");
        FUNC_END;
        return status;
    }

    rRequestNo   = mWaitRequestNo;
    rvResultMeta = mResultMeta;
    rpHeap       = mResultHeap;
    //
    mResultHeap = NULL;

    FUNC_END;
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
status_t
ZsdFlashSelector::
returnBuffer(
    android::sp<IImageBufferHeap>   rpHeap
)
{
    FUNC_START;
    Mutex::Autolock _l(mResultSetLock);
    MY_LOGD("In ZsdFlashSelector");
    sp<IConsumerPool> pPool = mpPool.promote();
    if ( pPool == 0 ) {
        MY_LOGE("Cannot promote consumer pool to return buffer.");
        return UNKNOWN_ERROR;
    }

    MY_LOGD_IF( 1, "return buf:%p", rpHeap.get());
    FUNC_END;
    return pPool->returnBuffer( rpHeap );
}

/******************************************************************************
 *
 ******************************************************************************/
status_t
ZsdFlashSelector::
returnMeta(Vector<MetaItemSet>& resultMeta)
{
    FUNC_START;
    MY_LOGD("In ZsdFlashSelector");
    for(int i=0 ; i < resultMeta.size() ; i++)
    {
        if(resultMeta[i].id == NSCam::eSTREAMID_META_HAL_DYNAMIC_P1)
        {
            IMetadata::Memory lcei_mem;
            if( tryGetMetadata<IMetadata::Memory>(&resultMeta[i].meta, MTK_VHDR_LCEI_DATA, lcei_mem) ) {
                if (lcei_mem.size() == sizeof(LCEI_CONFIG_DATA)){
                    LCEI_CONFIG_DATA* lceiConfig = (LCEI_CONFIG_DATA*)lcei_mem.array();
                    lceiConfig->FnReleaseHandle(lceiConfig->bufID, lceiConfig->halObj);
                    MY_LOGD_IF(1, "[LCSO] return Buffer LCEI, addr = %p ID(%d)", lceiConfig->lcsoBuf, lceiConfig->bufID);
                }else{
                    MY_LOGD_IF(1,"[LCSO] zsd Select lcei metatdata size != LCEI_CONFIG_DATA");
                }
             }else{
                MY_LOGD_IF(1,"[LCSO] no entry in zsd Selec for MTK_VHDR_LCEI_DATA");
            }
        }
    }
    FUNC_END;
    return 0;
}

/******************************************************************************
 *
 ******************************************************************************/
status_t
ZsdFlashSelector::
flush()
{
    FUNC_START;
    Mutex::Autolock _l(mResultSetLock);
    MY_LOGD("In ZsdFlashSelector");

    if ( mResultHeap!=NULL ) {
        returnMeta(mResultMeta);
        sp<IConsumerPool> pPool = mpPool.promote();
        if ( pPool == 0 ) {
            MY_LOGE("Cannot promote consumer pool to return buffer.");
            FUNC_END;
            return UNKNOWN_ERROR;
        }
        pPool->returnBuffer(mResultHeap);
    }

    FUNC_END;
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
void
ZsdFlashSelector::
onLastStrongRef( const void* /*id*/)
{
    FUNC_START;
    MY_LOGD("In ZsdFlashSelector");
    flush();
    FUNC_END;
}

/******************************************************************************
 *
 ******************************************************************************/
void
ZsdFlashSelector::
setWaitRequestNo(MUINT32 requestNo)
{
    MY_LOGD("setWaitRequestNo(%d)",requestNo);
    mWaitRequestNo = requestNo;
}


/******************************************************************************
 *
 ******************************************************************************/
VssSelector::
VssSelector()
{
    mLogLevel = property_get_int32("debug.camera.log", 0);
    if ( mLogLevel == 0 ) {
        mLogLevel = property_get_int32("debug.camera.log.selector", 1);
    }
}

/******************************************************************************
 *
 ******************************************************************************/
android::status_t
VssSelector::
selectResult(
    MINT32                             rRequestNo,
    Vector<MetaItemSet>                rvResult,
    android::sp<IImageBufferHeap>      rpHeap,
    MBOOL                              errorResult
)
{
    Mutex::Autolock _l(mResultSetLock);

    mResultSetMap.add( ResultSet_T{rRequestNo, rpHeap, rvResult} );

    MY_LOGD_IF( mLogLevel >= 1, "mResultSetMap.size:%d", mResultSetMap.size());
#if 1
    for ( size_t i = 0; i < mResultSetMap.size(); ++i )
        MY_LOGD_IF( mLogLevel >= 1, "mResultSetMap.size:%d request:%d", mResultSetMap.size(), mResultSetMap.editItemAt(i).requestNo );
#endif
    mCondResultSet.signal();
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
status_t
VssSelector::
getResult(
    MINT32&                          rRequestNo,
    Vector<MetaItemSet>&             rvResultMeta,
    android::sp<IImageBufferHeap>&   rpHeap
)
{

    Mutex::Autolock _l(mResultSetLock);
#define WAIT_BUFFER_TIMEOUT (3000000000) //ns
    if ( mResultSetMap.isEmpty() )
    {
        MY_LOGD("Wait result - E");
        mCondResultSet.waitRelative(mResultSetLock, WAIT_BUFFER_TIMEOUT);
        MY_LOGD("Wait result - X");
        if(mResultSetMap.isEmpty())
        {
            MY_LOGE("Time Out, no result can get.");
            return UNKNOWN_ERROR;
        }
    }
#undef WAIT_BUFFER_TIMEOUT
    rRequestNo   = mResultSetMap.editItemAt(0).requestNo;
    rvResultMeta = mResultSetMap.editItemAt(0).resultMeta;
    rpHeap       = mResultSetMap.editItemAt(0).heap;

    mResultSetMap.removeItemsAt(0);

    return OK;
}
/******************************************************************************
 *
 ******************************************************************************/
status_t
VssSelector::
returnBuffer(
    android::sp<IImageBufferHeap>   rpHeap
)
{
    FUNC_START;
    sp<IConsumerPool> pPool = mpPool.promote();
    if ( pPool == 0 )
    {
        MY_LOGE("Cannot promote consumer pool to return buffer.");
        return UNKNOWN_ERROR;
    }
    FUNC_END;
    return pPool->returnBuffer( rpHeap );
}

/******************************************************************************
 *
 ******************************************************************************/
status_t
VssSelector::
flush()
{
    FUNC_START;
    Mutex::Autolock _l(mResultSetLock);

    if ( ! mResultSetMap.empty() ) {
        sp<IConsumerPool> pPool = mpPool.promote();
        if ( pPool == 0 )
        {
            MY_LOGE("Cannot promote consumer pool to return buffer.");
            return UNKNOWN_ERROR;
        }

        for ( size_t i = 0; i < mResultSetMap.size(); ++i )
        {
            pPool->returnBuffer( mResultSetMap.editItemAt(i).heap );
        }
    }

    mResultSetMap.clear();
    FUNC_END;
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
void
VssSelector::
onLastStrongRef( const void* /*id*/)
{
    FUNC_START;
    flush();
    FUNC_END;
}

