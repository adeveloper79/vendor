/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkCam/ZsdRequestSelector"
//
#include "MyUtils.h"
//
#include <utils/RWLock.h>
//
#include <ImageBufferHeap.h>
#include <v3/utils/streaminfo/ImageStreamInfo.h>
#include <v3/stream/IStreamInfo.h>
//
#include <LegacyPipeline/buffer/Selector.h>
//
#include <camera_custom_zsd.h>


using namespace android;
using namespace NSCam;
using namespace NSCam::v1;
using namespace NSCam::v1::NSLegacyPipeline;
using namespace NSCam::Utils;

using namespace NSCam::v3::Utils;
/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("[%s] " fmt, __FUNCTION__, ##arg)
//
#define MY_LOGV_IF(cond, ...)       do { if ( (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if ( (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( (cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( (cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( (cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( (cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)

#if 0
#define FUNC_START     MY_LOGD("+")
#define FUNC_END       MY_LOGD("-")
#else
#define FUNC_START
#define FUNC_END
#endif


/******************************************************************************
 *
 ******************************************************************************/
ZsdRequestSelector::
ZsdRequestSelector()
    : mFlush(MFALSE)
{
    mLogLevel = ::property_get_int32("debug.camera.log", 0);
    if ( mLogLevel == 0 ) {
        mLogLevel = ::property_get_int32("debug.camera.log.zsdRselector", 0);
    }
}

/******************************************************************************
 *
 ******************************************************************************/
android::status_t
ZsdRequestSelector::
selectResult(
    MINT32                             rRequestNo,
    Vector<MetaItemSet>                rvResult,
    android::sp<IImageBufferHeap>      rpHeap,
    MBOOL                              errorResult
)
{
    FUNC_START;
    Mutex::Autolock _l(mResultSetLock);

    MY_LOGD_IF( 1 , "selectResult rRequestNo(%d) %p",rRequestNo, rpHeap.get());

    sp<IConsumerPool> pPool = mpPool.promote();
    if ( pPool == 0 ) {
        MY_LOGE("Cannot promote consumer pool to return buffer.");
        return UNKNOWN_ERROR;
    }

    if ( mFlush ) pPool->returnBuffer( rpHeap );

    MBOOL bFind = false;
    for ( size_t i = 0; i < mWaitRequestNo.size(); ++i ) {
        MINT32 req = mWaitRequestNo[i];
        if( req == rRequestNo) {
            bFind = true;
            mResultSet.add(
                ResultSet_t{
                    .requestNo  = rRequestNo,
                    .resultMeta = rvResult,
                    .resultHeap = rpHeap
                }
            );
            MY_LOGD_IF( 1 , "get result No(%d) %p",rRequestNo, rpHeap.get());
            mResultQueueCond.broadcast();
            break;
        }
    }

    if ( !bFind ) {
        android::sp<IImageBufferHeap> heapToReturn = mLatestResult.resultHeap;
        mLatestResult.requestNo  = rRequestNo;
        mLatestResult.resultMeta = rvResult;
        mLatestResult.resultHeap = rpHeap;
        if ( heapToReturn.get() ) pPool->returnBuffer( heapToReturn );

    }

    FUNC_END;
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
status_t
ZsdRequestSelector::
getResult(
    MINT32&                          rRequestNo,
    Vector<MetaItemSet>&             rvResultMeta,
    android::sp<IImageBufferHeap>&   rpHeap
)
{
    FUNC_START;
    Mutex::Autolock _l(mResultSetLock);

    MY_LOGD_IF( mLogLevel >= 1, "getResult wait....");

    nsecs_t timeout = 3000000000LL; //wait for 3 sec
    status_t status = mResultQueueCond.waitRelative(mResultSetLock,timeout);
    if(status != OK) {
        MY_LOGE("wait result timeout.... get latest result %d %p", mLatestResult.requestNo, mLatestResult.resultHeap.get());
        rRequestNo   = mLatestResult.requestNo;
        rvResultMeta = mLatestResult.resultMeta;
        rpHeap       = mLatestResult.resultHeap;
        //
        mLatestResult.resultHeap = nullptr;
        mLatestResult.resultMeta.clear();
        //
        FUNC_END;
        return OK;
    }

    rRequestNo   = mResultSet[0].requestNo;
    rvResultMeta = mResultSet[0].resultMeta;
    rpHeap       = mResultSet[0].resultHeap;
    //
    mResultSet.removeItemsAt(0);
    //
    sp<IConsumerPool> pPool = mpPool.promote();
    if ( pPool == 0 ) {
        MY_LOGE("Cannot promote consumer pool to return buffer.");
        return UNKNOWN_ERROR;
    }

    MY_LOGD_IF( mLogLevel >= 1, "return mLatestResult buf:%p", mLatestResult.resultHeap.get());
    pPool->returnBuffer( mLatestResult.resultHeap );

    mLatestResult.resultHeap = nullptr;
    mLatestResult.resultMeta.clear();

    MY_LOGD_IF( mLogLevel >= 1, "getResult %d %p", rRequestNo, rpHeap.get());

    FUNC_END;
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
status_t
ZsdRequestSelector::
returnBuffer(
    android::sp<IImageBufferHeap>   rpHeap
)
{
    FUNC_START;
    Mutex::Autolock _l(mResultSetLock);
    sp<IConsumerPool> pPool = mpPool.promote();
    if ( pPool == 0 ) {
        MY_LOGE("Cannot promote consumer pool to return buffer.");
        return UNKNOWN_ERROR;
    }

    MY_LOGD_IF( mLogLevel >= 1, "return buf:%p", rpHeap.get());
    FUNC_END;
    return pPool->returnBuffer( rpHeap );
}

/******************************************************************************
 *
 ******************************************************************************/
status_t
ZsdRequestSelector::
flush()
{
    FUNC_START;
    Mutex::Autolock _l(mResultSetLock);
    mFlush = MTRUE;

    sp<IConsumerPool> pPool;
    if ( !mResultSet.empty() ) {
        pPool = mpPool.promote();
        if ( pPool == nullptr ) {
            MY_LOGE("Cannot promote consumer pool to return buffer.");
            FUNC_END;
            return UNKNOWN_ERROR;
        }
    }

    for( size_t i = 0; i < mResultSet.size(); ++i ) {
        pPool->returnBuffer(mResultSet[i].resultHeap);
    }

    if ( mLatestResult.resultHeap != nullptr ) {
        pPool->returnBuffer( mLatestResult.resultHeap );
        mLatestResult.resultHeap = nullptr;
    }

    FUNC_END;
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
void
ZsdRequestSelector::
onLastStrongRef( const void* /*id*/)
{
    FUNC_START;
    flush();
    FUNC_END;
}

/******************************************************************************
 *
 ******************************************************************************/
void
ZsdRequestSelector::
setWaitRequestNo(Vector< MINT32 > requestNo)
{
    if ( requestNo.empty() ) return;
    MY_LOGD("mWaitRequestNo(%d) %d-%d", requestNo.size(), requestNo[0], requestNo[requestNo.size() - 1]);
    mWaitRequestNo = requestNo;
}