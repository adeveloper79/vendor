/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#define MTK_LOG_ENABLE 1
#include "camera_custom_zvhdr.h"
#include <math.h>
#include <cstdio>
#include <cstdlib>
#include <cutils/properties.h>
#include <cutils/log.h> // For XLOG?().
#include <utils/Errors.h>

#define MY_LOGD(fmt, arg...)  ALOGD(fmt, ##arg)
#define MY_LOGE(fmt, arg...)  ALOGE(fmt, ##arg)

#define MY_LOG_IF(cond, ...)      do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)

#ifndef CLAMP
#define CLAMP(x,min,max)                        (((x) > (max)) ? (max) : (((x) < (min)) ? (min) : (x)))
#endif


/**************************************************************************
 *                      D E F I N E S / M A C R O S                       *
 **************************************************************************/

// customize parameters
#define CUST_ZVHDR_8X_EXPTIME_50HZ              (70007)    //(50000)
#define CUST_ZVHDR_8X_EXPTIME_60HZ              (66670)     // (41667)
#define CUST_ZVHDR_8X_ISOTHRES                  (200)
#define CUST_ZVHDR_8X_RATIO                     (8)

#define CUST_ZVHDR_4X_EXPTIME_50HZ              (70007)    //(50000)
#define CUST_ZVHDR_4X_EXPTIME_60HZ              (66670)     // (41667)
#define CUST_ZVHDR_4X_ISOTHRES                  (200)
#define CUST_ZVHDR_4X_RATIO                     (4)

#define CUST_ZVHDR_2X_EXPTIME_50HZ              (70007)    //(50000)
#define CUST_ZVHDR_2X_EXPTIME_60HZ              (66670)     // (41667)
#define CUST_ZVHDR_2X_ISOTHRES                  (200)
#define CUST_ZVHDR_2X_RATIO                     (2)

#define CUST_ZVHDR_1X_EXPTIME_50HZ              (70007)    //(50000)
#define CUST_ZVHDR_1X_EXPTIME_60HZ              (66670)     // (41667)
#define CUST_ZVHDR_1X_ISOTHRES                  (400)
#define CUST_ZVHDR_1X_RATIO                     (1)


#define CUST_ZVHDR_ENABLE_WORKAROUND_SOLUTION   (0)
#define CUST_ZVHDR_WORKAROUND_ISOTHRES          (1200)
#define CUST_ZVHDR_MIN_EXPOSURE_THRES           (400)


/**************************************************************************
 *     E N U M / S T R U C T / T Y P E D E F    D E C L A R A T I O N     *
 **************************************************************************/
#define CUST_ZVHDR_ANTIBANDING_BASE_50HZ        (9995)
#define CUST_ZVHDR_ANTIBANDING_BASE_60HZ        (8335)
#define CUST_ZVHDR_ANTIBANDING_BASE_TOLERRANCE  (10)        // %

#define CUST_ZVHDR_ISPGAIN_BASE                 (1024)      // 1x = 1023
#define CUST_ZVHDR_LV_INDEX_UNIT                (10)        // 1.0 LV
#define CUST_ZVHDR_LV_INDEX_MIN                 (0)         // LV 0
#define CUST_ZVHDR_LV_INDEX_MAX                 (18)        // LV 18
#define CUST_ZVHDR_LV_INDEX_NUM                 ((CUST_ZVHDR_LV_INDEX_MAX - CUST_ZVHDR_LV_INDEX_MIN) + 1)


/*
*   Notice, we do not sugget use ratio = 8, since not all sensors support
*/
                                                    //LV 0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18
static MINT32 i4DynamicRatio[CUST_ZVHDR_LV_INDEX_NUM] = {1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 4, 4, 4, 4, 4, 4, 4};
                                                   //LV 0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18
static MINT32 i4DeFlickerLV[CUST_ZVHDR_LV_INDEX_NUM] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0};


/**************************************************************************
 *                 E X T E R N A L    R E F E R E N C E S                 *
 **************************************************************************/

/**************************************************************************
 *                         G L O B A L    D A T A                         *
 **************************************************************************/
static MBOOL bVHDRResetFlag = MFALSE;
static MINT32 i4NVRAMRMGSeg = 3072;
static MBOOL m_bDebugEnable = MFALSE;
static MBOOL m_bRMGTHSmoothEnable = MTRUE;

/**************************************************************************
 *       P R I V A T E    F U N C T I O N    D E C L A R A T I O N        *
 **************************************************************************/
MVOID getZVHDRExpSetting(const ZVHDRExpSettingInputParam_T& rInput, ZVHDRExpSettingOutputParam_T& rOutput)
{
    MUINT64 u8LEExpTimeUs;
    MUINT64 u8LEMaxExpTimeUs;
    MUINT64 u8ISOThresValue;
    MUINT64 u8TotalGain;
    MUINT32 u4AnitBandingBase;
    MUINT32 u4Ratio_x100;
    MUINT32 u4CountIdx = 0;
    MUINT32 u4NewExpTimeUs;
    MUINT32 u4ISOWorkaroundValue;
    MINT32 i4ISOThr;
    MINT32 i4HdrLV, i4Ratio, i4IsoRatio, i4FinalRatio, i4DeflickerEn, i4FlickerMultiple, i4RatioCheck;
    MINT32 i4LEShutter;
    MINT32 i4FlickerTolerance;

    char value[PROPERTY_VALUE_MAX] = {'\0'};
    property_get("debug.zvhdr-pline.enable", value, "0");
    m_bDebugEnable = (MBOOL)atoi(value);

    property_get("debug.zvhdr-rmgthsmooth.enable", value, "1");
    m_bRMGTHSmoothEnable = (MBOOL)atoi(value);

    i4HdrLV = (rInput.i4LV + (CUST_ZVHDR_LV_INDEX_UNIT >> 1)) / CUST_ZVHDR_LV_INDEX_UNIT;
    i4HdrLV = CLAMP(i4HdrLV, CUST_ZVHDR_LV_INDEX_MIN, CUST_ZVHDR_LV_INDEX_MAX);
    i4DeflickerEn = i4DeFlickerLV[i4HdrLV];
    i4Ratio = i4DynamicRatio[i4HdrLV];
    u4ISOWorkaroundValue = ((CUST_ZVHDR_WORKAROUND_ISOTHRES << 10) + (rInput.u41xGainISO >> 1)) / rInput.u41xGainISO;
    i4ISOThr = ((CUST_ZVHDR_WORKAROUND_ISOTHRES << 10) + (rInput.u41xGainISO >> 1)) / rInput.u41xGainISO;

    u8ISOThresValue = (rInput.u4ISPGain * rInput.u4SensorGain) >> 10;

    if(u8ISOThresValue >= u4ISOWorkaroundValue) {
        MY_LOG_IF(m_bDebugEnable, "[getZVHDRExpSetting] Iso > IsoThr (%lld, %d)\n", u8ISOThresValue, u4ISOWorkaroundValue);
        i4IsoRatio = 1;
    }
    else{
        MY_LOG_IF(m_bDebugEnable, "[getZVHDRExpSettingg] Iso < IsoThr (%lld, %d)\n", u8ISOThresValue, u4ISOWorkaroundValue);
        i4IsoRatio = i4Ratio;
    }

    if (!m_bRMGTHSmoothEnable) {
        if(i4IsoRatio == 1)
            i4IsoRatio = 2;
    }

    switch(i4IsoRatio)
    {
        case CUST_ZVHDR_8X_RATIO:
            (rInput.bIs60HZ == MTRUE) ? (u8LEExpTimeUs = CUST_ZVHDR_8X_EXPTIME_60HZ)
                                      : (u8LEExpTimeUs = CUST_ZVHDR_8X_EXPTIME_50HZ);
            break;

        case CUST_ZVHDR_4X_RATIO:
            (rInput.bIs60HZ == MTRUE) ? (u8LEExpTimeUs = CUST_ZVHDR_4X_EXPTIME_60HZ)
                                      : (u8LEExpTimeUs = CUST_ZVHDR_4X_EXPTIME_50HZ);
            break;

        case CUST_ZVHDR_2X_RATIO:
            (rInput.bIs60HZ == MTRUE) ? (u8LEExpTimeUs = CUST_ZVHDR_2X_EXPTIME_60HZ)
                                      : (u8LEExpTimeUs = CUST_ZVHDR_2X_EXPTIME_50HZ);
            break;

        default:
            (rInput.bIs60HZ == MTRUE) ? (u8LEExpTimeUs = CUST_ZVHDR_1X_EXPTIME_60HZ)
                                      : (u8LEExpTimeUs = CUST_ZVHDR_1X_EXPTIME_50HZ);

    }



    (rInput.bIs60HZ == MTRUE) ? (u4AnitBandingBase = CUST_ZVHDR_ANTIBANDING_BASE_60HZ)
                              : (u4AnitBandingBase = CUST_ZVHDR_ANTIBANDING_BASE_50HZ);


    i4FlickerTolerance = ((MINT32)u4AnitBandingBase * CUST_ZVHDR_ANTIBANDING_BASE_TOLERRANCE + 50) / 100;

    i4LEShutter = rInput.u4ShutterTime;

    if(rInput.u4ShutterTime < CUST_ZVHDR_MIN_EXPOSURE_THRES)
    {
        i4LEShutter = CUST_ZVHDR_MIN_EXPOSURE_THRES;
    }

    u8TotalGain = (MUINT64)i4LEShutter * (MUINT64)rInput.u4SensorGain * (MUINT64)rInput.u4ISPGain ;

    rOutput.bEnableWorkaround = MFALSE;
    rOutput.u4LESensorGain = rInput.u4SensorGain;

    MY_LOG_IF(m_bDebugEnable, "[getZVHDRExpSetting] Freq:%d, Lv:%d, x1ISO:%d, SatGain:%d, SensorGain:%d, ISPGain:%d, InputShut:%d, LEShut:%d\n", rInput.bIs60HZ, rInput.i4LV,
                                                                                                              rInput.u41xGainISO, rInput.u4SaturationGain,
                                                                                                              rInput.u4SensorGain, rInput.u4ISPGain, rInput.u4ShutterTime, i4LEShutter);
    MY_LOG_IF(m_bDebugEnable, "[getZVHDRExpSetting] Exp:%lld, Iso:%lld, MaxExp:%lld, AntiBandingBase:%d\n", u8LEExpTimeUs, u8ISOThresValue,
                                                                                                            u8LEMaxExpTimeUs, u4AnitBandingBase);
    MY_LOG_IF(m_bDebugEnable, "[getZVHDRExpSetting] TotalGain:%lld\n", u8TotalGain);

    if(i4LEShutter > u8LEExpTimeUs)
    {
        rOutput.u4LEExpTimeInUS = u8LEExpTimeUs;
        u8TotalGain = (u8TotalGain + ((MUINT64)rOutput.u4LEExpTimeInUS >> 1)) / (MUINT64)rOutput.u4LEExpTimeInUS;
        rOutput.u4LEISPGain = (u8TotalGain + ((MUINT64)rOutput.u4LESensorGain >> 1)) / (MUINT64)rOutput.u4LESensorGain;

    }
    else {
        rOutput.u4LEExpTimeInUS = i4LEShutter;
        rOutput.u4LEISPGain = rInput.u4ISPGain;
    }

    rOutput.u4SEExpTimeInUS = rOutput.u4LEExpTimeInUS / i4IsoRatio;
    rOutput.u4SESensorGain = rOutput.u4LESensorGain;
    rOutput.u4SEISPGain = rOutput.u4LEISPGain;

    if(i4DeflickerEn) {
        i4FlickerMultiple = rOutput.u4SEExpTimeInUS / u4AnitBandingBase;
        rOutput.u4SEExpTimeInUS = i4FlickerMultiple * u4AnitBandingBase;

        if (!m_bRMGTHSmoothEnable) {
            i4RatioCheck = (rOutput.u4LEExpTimeInUS * 100) / rOutput.u4SEExpTimeInUS;
            i4RatioCheck = (i4RatioCheck + 50) / 100;

            /*
            *   If Ratio equals 1, change ratio to 2 to prevent RMG hunting
            */
            if(i4RatioCheck == 1)
            {
                rOutput.u4SEExpTimeInUS = rOutput.u4LEExpTimeInUS / CUST_ZVHDR_2X_RATIO;
            }
        }

        if(rOutput.u4SEExpTimeInUS < u4AnitBandingBase)
            rOutput.u4SEExpTimeInUS = u4AnitBandingBase;

        /*
        *   If LE < BandingBase, no need to follow flicker rule
        */
        if(rOutput.u4LEExpTimeInUS < (u4AnitBandingBase - i4FlickerTolerance))
        {
            rOutput.u4SEExpTimeInUS = rOutput.u4LEExpTimeInUS / i4IsoRatio;
        }

        i4FinalRatio = (rOutput.u4LEExpTimeInUS * 100) / rOutput.u4SEExpTimeInUS;
        MY_LOG_IF(m_bDebugEnable, "[getZVHDRExpSetting] i4DeflickerEn:1, RatioCheck:%d, FinaleRataio:%d, LE:%d, SE:%d, FlkMult:%d, FlkTolerance:%d\n", i4RatioCheck, i4FinalRatio, rOutput.u4LEExpTimeInUS, rOutput.u4SEExpTimeInUS, i4FlickerMultiple, i4FlickerTolerance);

    }
    else
    {
        MY_LOG_IF(m_bDebugEnable, "[getZVHDRExpSetting] i4DeflickerEn=0\n");
        i4FinalRatio = i4IsoRatio * 100;
    }

    if(rOutput.u4LEISPGain >= u4ISOWorkaroundValue) {
        rOutput.u4LEISPGain = u4ISOWorkaroundValue;
        rOutput.u4SEISPGain = u4ISOWorkaroundValue;
    }

    /*
    * Ratio Bound Protection
    */
    if(i4FinalRatio < 100) {
        i4FinalRatio = 100;
        rOutput.u4SEExpTimeInUS = rOutput.u4LEExpTimeInUS;
    }

    rOutput.i4rvHdrRMGSeg = i4NVRAMRMGSeg;

    MY_LOG_IF(m_bDebugEnable, "[getZVHDRExpSetting2] TargetRatio: %d, IsoRatio: %d, FinalRatio:%d, FlickerEn:%d, i4LV:%d, LE:(%d, %d, %d), SE:(%d, %d, %d)\n",
                                                                                           i4Ratio, i4IsoRatio, i4FinalRatio,
                                                                                           i4DeflickerEn, rInput.i4LV,
                                                                                           rOutput.u4LEExpTimeInUS, rOutput.u4LESensorGain, rOutput.u4LEISPGain,
                                                                                           rOutput.u4SEExpTimeInUS, rOutput.u4SESensorGain, rOutput.u4SEISPGain);


    if (m_bRMGTHSmoothEnable) {
        RMGTHSmooth(i4FinalRatio, rOutput, u4AnitBandingBase);
    }

    rOutput.u4LE_SERatio_x100 = rOutput.u4LEExpTimeInUS *100 / rOutput.u4SEExpTimeInUS;
    if(rOutput.u4LE_SERatio_x100 < 100) {
        rOutput.u4LE_SERatio_x100 = 100;
        rOutput.u4SEExpTimeInUS = rOutput.u4LEExpTimeInUS;
    }

    MY_LOG_IF(m_bDebugEnable, "[getZVHDRExpSetting3] RMGTHSmoothEnable: %d, RMG_TH: %d, u4LE_SERatio_x100: %d, (LE,SE)=(%d, %d)\n", m_bRMGTHSmoothEnable, rOutput.i4rvHdrRMGSeg, rOutput.u4LE_SERatio_x100,rOutput.u4LEExpTimeInUS,rOutput.u4SEExpTimeInUS);

}

MVOID resetZVHDRFlag(const MINT32 i4vRMGSeg){
    bVHDRResetFlag = MTRUE;
    i4NVRAMRMGSeg = i4vRMGSeg;
}


/**********************************************************************************
 * Dynamic ratio 2x <-> 1x with RMG_TH change                                     *
 *                                                                                *
 * Luminance level changes when LE/SE ratio change from 2x to 1x and vice versa   *
 *                                                                                *
 * (1) When change from 2x to 1x, slowly increase non-linear RMG k value to 4090  *
 *     then change setting to 1x.                                                 *
 * (2) When change from 1x to 2x, change setting to 2x
with non-linear RMG_TH     *
 *     value 4090 then slowly decrease non-linear RMG_TH value from 4090 to target*
 *     value then change setting to 1x.                                           *
 **********************************************************************************/
MVOID RMGTHSmooth(MINT32 i4FinalRatio, ZVHDRExpSettingOutputParam_T& rOutput, MINT32 u4AnitBandingBase) {


    MINT32 u4RMGIdxRange;
    const MINT32 u4SegUnit = 128;
    static MINT32 u4PvLSRatiox100 = 0;
    static MINT32 u4count = 0;
    MINT32 u4TargetSEExpTimeInUS = 0;

    u4RMGIdxRange = (4096-i4NVRAMRMGSeg)/u4SegUnit;

    if (bVHDRResetFlag == MTRUE) {
        u4PvLSRatiox100 = 0;
        u4count = 0;
        bVHDRResetFlag = MFALSE;
    }

    MY_LOG_IF(m_bDebugEnable, "[%s] init u4PvLSRatiox100: %d, u4count: %d\n", __FUNCTION__, u4PvLSRatiox100,u4count);
    if ((u4PvLSRatiox100 != 0) && (i4FinalRatio == 100)) {
        if (u4count < u4RMGIdxRange) {
            u4TargetSEExpTimeInUS = rOutput.u4LEExpTimeInUS*100/u4PvLSRatiox100;

            if (u4TargetSEExpTimeInUS < u4AnitBandingBase) {
                rOutput.i4rvHdrRMGSeg = i4NVRAMRMGSeg;
                u4PvLSRatiox100 = i4FinalRatio;
                u4count = u4RMGIdxRange;

            } else {
                rOutput.u4SEExpTimeInUS = u4TargetSEExpTimeInUS;
                if (u4count == u4RMGIdxRange - 1) {
                    rOutput.i4rvHdrRMGSeg = 4090;
                } else {
                    rOutput.i4rvHdrRMGSeg = i4NVRAMRMGSeg + (u4count+1)*u4SegUnit;
                }
                u4count++;
            }
        } else {
            rOutput.i4rvHdrRMGSeg = i4NVRAMRMGSeg;
            u4PvLSRatiox100 = i4FinalRatio;
        }
    } else if (i4FinalRatio != 100){
        if (u4count > 0) {
            rOutput.u4SEExpTimeInUS = rOutput.u4LEExpTimeInUS*100/i4FinalRatio;
            u4PvLSRatiox100 = i4FinalRatio;
            if (u4count == u4RMGIdxRange) {
                rOutput.i4rvHdrRMGSeg = 4090;
            } else {
                rOutput.i4rvHdrRMGSeg = i4NVRAMRMGSeg + u4count*u4SegUnit;
            }
            u4count--;
        } else {
            rOutput.i4rvHdrRMGSeg = i4NVRAMRMGSeg;
            u4PvLSRatiox100 = i4FinalRatio;
        }
    } else {
        rOutput.i4rvHdrRMGSeg = i4NVRAMRMGSeg;
        u4PvLSRatiox100 = i4FinalRatio;
        u4count = u4RMGIdxRange;
    }

    MY_LOG_IF(m_bDebugEnable, "[%s] u4PvLSRatiox100: %d, i4rvHdrRMGSeg: %d, u4count: %d, LE: %d, SE: %d\n", __FUNCTION__, u4PvLSRatiox100, rOutput.i4rvHdrRMGSeg, u4count, rOutput.u4LEExpTimeInUS, rOutput.u4SEExpTimeInUS);
}

