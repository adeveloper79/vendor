#include <pd_buf_common.h>

#if MTK_CAM_HAVE_DUALPD_SUPPORT
const DUALPD_VC_SETTING_T dualPDVCSetting[DualPD_VCBuf_Type_Num] = {{2, 4, 3},
                                                                    {4, 4, 4}};
#endif
