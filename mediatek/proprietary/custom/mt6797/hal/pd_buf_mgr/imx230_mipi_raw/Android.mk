LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
LOCAL_IS_HOST_MODULE :=
LOCAL_MODULE := libSonyIMX230PdafLibrary
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_TAGS := optional
#OVERRIDE_BUILT_MODULE_PATH :=$(PRODUCT_OUT)/obj/lib
LOCAL_UNINSTALLABLE_MODULE :=
LOCAL_SRC_FILES_arm := 32/libSonyIMX230PdafLibrary.so
LOCAL_SRC_FILES_arm64 := 64/libSonyIMX230PdafLibrary.so
#LOCAL_BUILT_MODULE_STEM := libSonyIMX230PdafLibrary.so
LOCAL_STRIP_MODULE :=
#LOCAL_MODULE_STEM := libSonyIMX230PdafLibrary.so
LOCAL_CERTIFICATE :=
#LOCAL_MODULE_PATH := $(PRODUCT_OUT)/system/lib
LOCAL_REQUIRED_MODULE :=
LOCAL_SHARED_LIBRARIES :=
LOCAL_MODULE_SUFFIX := .so
LOCAL_MULTILIB := both
include $(BUILD_PREBUILT)

