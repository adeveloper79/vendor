[single_bin]
logo.bin=logo
lk.bin=lk
boot.img=boot
recovery.img=recovery
md1arm7.img=md1arm7
md1dsp.img=md1dsp
md1rom.img=md1rom
md3rom.img=md3rom

[multi_bin]
trustzone.bin=atf,tee
tinysys-scp.bin=tinysys-loader-CM4_A,tinysys-scp-CM4_A
