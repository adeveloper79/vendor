#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "arsi_api.h"


typedef struct {
    uint32_t param1;
    uint32_t param2;
} my_private_enh_param_t;

typedef struct {
    arsi_task_config_t task_config;
    uint32_t tmp_buf_size;
    uint32_t my_private_var;
    debug_log_fp_t debug_log;
} my_private_handler_t;


lib_status_t arsi_query_working_buf_size(
    const arsi_task_config_t *p_arsi_task_config,
    uint32_t                 *p_working_buf_size)
{
    /* calculate working buffer working_buf_size by task_config */
    p_arsi_task_config->debug_log("%s(), p_arsi_task_config->task_scene = %u\n",
                                  __func__, p_arsi_task_config->task_scene);
    p_arsi_task_config->debug_log("%s(), p_arsi_task_config->frame_size_ms = %u\n",
                                  __func__, p_arsi_task_config->frame_size_ms);

    p_arsi_task_config->debug_log("%s(), p_arsi_task_config->stream_uplink.device = %u\n",
                                  __func__, p_arsi_task_config->stream_uplink.device);

    p_arsi_task_config->debug_log("%s(), p_arsi_task_config->stream_uplink.sample_rate_in = %lu\n",
                                  __func__, p_arsi_task_config->stream_uplink.sample_rate_in);
    p_arsi_task_config->debug_log("%s(), p_arsi_task_config->stream_uplink.sample_rate_out = %lu\n",
                                  __func__, p_arsi_task_config->stream_uplink.sample_rate_out);

    p_arsi_task_config->debug_log("%s(), p_arsi_task_config->stream_uplink.bit_format_in = %d\n",
                                  __func__, p_arsi_task_config->stream_uplink.bit_format_in);
    p_arsi_task_config->debug_log("%s(), p_arsi_task_config->stream_uplink.bit_format_out = %d\n",
                                  __func__, p_arsi_task_config->stream_uplink.bit_format_out);

    p_arsi_task_config->debug_log("%s(), p_arsi_task_config->stream_uplink.num_channels_in = %d\n",
                                  __func__, p_arsi_task_config->stream_uplink.num_channels_in);
    p_arsi_task_config->debug_log("%s(), p_arsi_task_config->stream_uplink.num_channels_out = %d\n",
                                  __func__, p_arsi_task_config->stream_uplink.num_channels_out);


    p_arsi_task_config->debug_log("%s(), p_arsi_task_config->stream_downlink.device = %d\n",
                                  __func__, p_arsi_task_config->stream_downlink.device);

    p_arsi_task_config->debug_log("%s(), p_arsi_task_config->stream_downlink.sample_rate_in = %lu\n",
                                  __func__, p_arsi_task_config->stream_downlink.sample_rate_in);
    p_arsi_task_config->debug_log("%s(), p_arsi_task_config->stream_downlink.sample_rate_out = %lu\n",
                                  __func__, p_arsi_task_config->stream_downlink.sample_rate_out);

    p_arsi_task_config->debug_log("%s(), p_arsi_task_config->stream_downlink.bit_format_in = %d\n",
                                  __func__, p_arsi_task_config->stream_downlink.bit_format_in);
    p_arsi_task_config->debug_log("%s(), p_arsi_task_config->stream_downlink.bit_format_out = %d\n",
                                  __func__, p_arsi_task_config->stream_downlink.bit_format_out);

    p_arsi_task_config->debug_log("%s(), p_arsi_task_config->stream_downlink.num_channels_in = %d\n",
                                  __func__, p_arsi_task_config->stream_downlink.num_channels_in);
    p_arsi_task_config->debug_log("%s(), p_arsi_task_config->stream_downlink.num_channels_out = %d\n",
                                  __func__, p_arsi_task_config->stream_downlink.num_channels_out);

    *p_working_buf_size = sizeof(my_private_handler_t);

    p_arsi_task_config->debug_log("%s(), working_buf_size = %lu\n", __func__,
                                  *p_working_buf_size);
    return LIB_OK;
}


lib_status_t arsi_create_handler(
    const arsi_task_config_t *p_arsi_task_config,
    const data_buf_t         *p_param_buf,
    data_buf_t               *p_working_buf,
    void                    **pp_handler)
{
    p_arsi_task_config->debug_log("%s()\n", __func__);

    /* init handler by task_config */
    *pp_handler = p_working_buf->p_buffer;

    my_private_handler_t *my_private_handler = (my_private_handler_t *)*pp_handler;

    memcpy(&my_private_handler->task_config,
           p_arsi_task_config,
           sizeof(arsi_task_config_t));
    my_private_handler->tmp_buf_size = 640;
    my_private_handler->my_private_var = 0x5566;
    my_private_handler->debug_log = p_arsi_task_config->debug_log;

    return LIB_OK;
}


lib_status_t arsi_process_ul_buf(
    audio_buf_t    *p_ul_buf_in,
    audio_buf_t    *p_ul_buf_out,
    audio_buf_t    *p_aec_buf_in,
    const uint32_t  delay_ms,
    void           *p_handler,
    void           *arg)
{
    my_private_handler_t *my_private_handler = (my_private_handler_t *)p_handler;
    extra_call_arg_t *p_extra_call_arg = (extra_call_arg_t *)arg;

    memcpy(
        p_ul_buf_out->pcm_data.p_buffer,
        p_ul_buf_in->pcm_data.p_buffer,
        p_ul_buf_out->pcm_data.memory_size);
    // p_ul_buf_in->pcm_data.data_size >= p_ul_buf_out->pcm_data.memory_size

    p_ul_buf_in->pcm_data.data_size = 0; // full used
    p_ul_buf_out->pcm_data.data_size = p_ul_buf_out->pcm_data.memory_size; // filled
    p_aec_buf_in->pcm_data.data_size = 0; // full used

    my_private_handler->debug_log("call_band_type = %d, call_band_type = %d\n",
                                  p_extra_call_arg->call_band_type,
                                  p_extra_call_arg->call_net_type);

    return LIB_OK;
}


lib_status_t arsi_process_dl_buf(
    audio_buf_t    *p_dl_buf_in,
    audio_buf_t    *p_dl_buf_out,
    void           *p_handler,
    void           *arg)
{
    my_private_handler_t *my_private_handler = (my_private_handler_t *)p_handler;
    extra_call_arg_t *p_extra_call_arg = (extra_call_arg_t *)arg;

    memcpy(
        p_dl_buf_out->pcm_data.p_buffer,
        p_dl_buf_in->pcm_data.p_buffer,
        p_dl_buf_out->pcm_data.memory_size);

    p_dl_buf_in->pcm_data.data_size = 0; // full used
    p_dl_buf_out->pcm_data.data_size = p_dl_buf_out->pcm_data.memory_size;

    my_private_handler->debug_log("call_band_type = %d, call_band_type = %d\n",
                                  p_extra_call_arg->call_band_type,
                                  p_extra_call_arg->call_net_type);

    return LIB_OK;
}


lib_status_t arsi_destroy_handler(void *p_handler)
{
    my_private_handler_t *my_private_handler = (my_private_handler_t *)p_handler;
    my_private_handler->debug_log("%s(), p_handler = %p\n", __func__,
                                  p_handler);
    return LIB_OK;
}


lib_status_t arsi_update_device(
    const arsi_task_config_t *p_arsi_task_config,
    const data_buf_t         *p_param_buf,
    void                     *p_handler)
{
    my_private_handler_t *my_private_handler = (my_private_handler_t *)p_handler;

    my_private_handler->debug_log("%s(), my_private_handler->my_private_var = 0x%lx\n",
                                  __func__, my_private_handler->my_private_var);

    task_device_in_t device_in_old =
        my_private_handler->task_config.stream_uplink.device;
    task_device_in_t device_in_new =
        p_arsi_task_config->stream_uplink.device;

    task_device_out_t device_out_old =
        my_private_handler->task_config.stream_uplink.device;
    task_device_out_t device_out_new =
        p_arsi_task_config->stream_uplink.device;

    my_private_handler->debug_log("%s(), input device: 0x%x => 0x%x\n",
                                  __func__,
                                  device_in_old,
                                  device_in_new);
    my_private_handler->debug_log("%s(), output device: 0x%x => 0x%x\n",
                                  __func__,
                                  device_out_old,
                                  device_out_new);

    return LIB_OK;
}


lib_status_t arsi_update_param(
    const arsi_task_config_t *p_arsi_task_config,
    const data_buf_t         *p_param_buf,
    void                     *p_handler)
{
    my_private_handler_t *my_private_handler = (my_private_handler_t *)p_handler;

    my_private_handler->debug_log("%s(), my_private_handler->my_private_var = 0x%lx\n",
                                  __func__,
                                  my_private_handler->my_private_var);

    /* pick up new param and update handler */

    return LIB_OK;
}


lib_status_t arsi_query_param_buf_size(
    const arsi_task_config_t *p_arsi_task_config,
    const string_buf_t       *platform_name,
    const string_buf_t       *param_file_path,
    const int                 enhancement_mode,
    uint32_t                 *p_param_buf_size)
{
    *p_param_buf_size = sizeof(my_private_enh_param_t);
    p_arsi_task_config->debug_log("%s(), get param buf size %lu for speech mode %d from file %s\n",
                                  __func__, *p_param_buf_size, enhancement_mode, param_file_path->p_string);

    return LIB_OK;
}


lib_status_t arsi_parsing_param_file(
    const arsi_task_config_t *p_arsi_task_config,
    const string_buf_t       *platform_name,
    const string_buf_t       *param_file_path,
    const int                 enhancement_mode,
    data_buf_t               *p_param_buf)
{
    my_private_enh_param_t *p_my_private_enh_param = NULL;

    p_arsi_task_config->debug_log("%s(), parsing file %s...\n", __func__,
                                  param_file_path->p_string);

    p_my_private_enh_param = (my_private_enh_param_t *)p_param_buf;

    p_my_private_enh_param->param1 = 0x12; // should get param from param_file_path
    p_my_private_enh_param->param2 = 0x34; // should get param from param_file_path

    return LIB_OK;
}


lib_status_t arsi_set_addr_value(
    const uint32_t addr,
    const uint32_t value,
    void          *p_handler)
{
    my_private_handler_t *my_private_handler = (my_private_handler_t *)p_handler;
    my_private_handler->debug_log("%s(), set value %lu at addr %p\n", __func__,
                                  value, (void *)addr);
    return LIB_OK;
}


lib_status_t arsi_get_addr_value(
    const uint32_t addr,
    uint32_t      *p_value,
    void          *p_handler)
{
    my_private_handler_t *my_private_handler = (my_private_handler_t *)p_handler;
    *p_value = 0x1234; // should get param from handler

    my_private_handler->debug_log("%s(), value %lu at addr %p\n", __func__,
                                  *p_value, (void *)addr);
    return LIB_OK;
}


lib_status_t arsi_set_key_value_pair(
    const string_buf_t *key_value_pair,
    void               *p_handler)
{
    my_private_handler_t *my_private_handler = (my_private_handler_t *)p_handler;
    my_private_handler->debug_log("%s(), key value pair = %s\n", __func__,
                                  key_value_pair->p_string);
    return LIB_OK;
}


lib_status_t arsi_get_key_value_pair(
    string_buf_t *key_value_pair,
    void         *p_handler)
{
    my_private_handler_t *my_private_handler = (my_private_handler_t *)p_handler;
    my_private_handler->debug_log("+%s(), key = %s\n", __func__,
                                  key_value_pair->p_string);

    if (strcmp(key_value_pair->p_string, "HAHA") == 0) {


    }

    my_private_handler->debug_log("-%s(), key value pair = %s\n", __func__,
                                  key_value_pair->p_string);
    return LIB_OK;
}


lib_status_t arsi_set_ul_digital_gain(
    const int16_t ul_analog_gain_ref_only,
    const int16_t ul_digital_gain,
    void         *p_handler)
{
    my_private_handler_t *my_private_handler = (my_private_handler_t *)p_handler;
    my_private_handler->debug_log("%s(), ul_digital_gain = %d\n", __func__,
                                  ul_digital_gain);
    return LIB_OK;
}


lib_status_t arsi_set_dl_digital_gain(
    const int16_t dl_analog_gain_ref_only,
    const int16_t dl_digital_gain,
    void         *p_handler)
{
    my_private_handler_t *my_private_handler = (my_private_handler_t *)p_handler;
    my_private_handler->debug_log("%s(), dl_digital_gain = %d\n", __func__,
                                  dl_digital_gain);
    return LIB_OK;
}


lib_status_t arsi_set_ul_mute(const uint8_t b_mute_on, void *p_handler)
{
    my_private_handler_t *my_private_handler = (my_private_handler_t *)p_handler;
    my_private_handler->debug_log("%s(), b_mute_on = %d\n", __func__,
                                  b_mute_on);
    return LIB_OK;
}


lib_status_t arsi_set_dl_mute(const uint8_t b_mute_on, void *p_handler)
{
    my_private_handler_t *my_private_handler = (my_private_handler_t *)p_handler;
    my_private_handler->debug_log("%s(), b_mute_on = %d\n", __func__,
                                  b_mute_on);
    return LIB_OK;
}


lib_status_t arsi_set_ul_enhance(const uint8_t b_enhance_on, void *p_handler)
{
    my_private_handler_t *my_private_handler = (my_private_handler_t *)p_handler;
    my_private_handler->debug_log("%s(), b_enhance_on = %d\n", __func__,
                                  b_enhance_on);
    return LIB_OK;
}


lib_status_t arsi_set_dl_enhance(const uint8_t b_enhance_on, void *p_handler)
{
    my_private_handler_t *my_private_handler = (my_private_handler_t *)p_handler;
    my_private_handler->debug_log("%s(), b_enhance_on = %d\n", __func__,
                                  b_enhance_on);
    return LIB_OK;
}


lib_status_t arsi_set_debug_log_fp(const debug_log_fp_t debug_log,
                                   void *p_handler)
{
    my_private_handler_t *my_private_handler = (my_private_handler_t *)p_handler;
    my_private_handler->debug_log = debug_log;
    return LIB_OK;
}

