/*
    Here is an interface of the backtrace to hide the platform-dependent libraries. 
*/

#ifndef __BACK_TRACE_H__
#define __BACK_TRACE_H__

// Type definitions
#ifdef __LINUX_PLAT__
#ifdef __ANDROID__
typedef struct back_trace_arg back_trace_arg_t;
typedef struct back_trace_arg* back_trace_arg_ptr_t;
#endif
#endif

// Macros
#define BACK_TRACE_SIZE	(16)    // NEED_TO_BE_NOTICED, NOT exceeding 2 digits
#define BACK_TRACE_NULL_DEMANGLE    "<unknown>"

// API
extern void back_trace_dump (void);

// Implementation
// => Internal data type for the pool
#ifdef __LINUX_PLAT__
#ifdef __ANDROID__
struct back_trace_arg
{
    void **curr;
    void **end;
};
#endif
#endif

#endif
