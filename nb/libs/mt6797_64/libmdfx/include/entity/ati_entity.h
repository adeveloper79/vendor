#ifndef __ENTITY_ATI_ENTITY_H__
#define __ENTITY_ATI_ENTITY_H__

#include "context.h"
#include "context/task.h"
#include "event.h"
#include "ctnr/chain.h"

// Compiler flags, NEED_TO_BE_NOTICED, set by the compiler
// N/A

// Type definitions
// ==== Legacy ====
typedef struct ati_reg_tbl_entry ati_reg_tbl_entry_t;
typedef struct ati_reg_tbl_entry* ati_reg_tbl_entry_ptr_t;
typedef struct ati_reg_req ati_reg_req_t;
typedef struct ati_reg_req* ati_reg_req_ptr_t;
typedef void* (*ati_atc_hdl_t) (ati_reg_tbl_entry_ptr_t entry_ptr, const char *atc, void *arg);
// ==== Legacy ====
#if 0
typedef struct atmngr_subscribe_req atmngr_subscribe_req_t;
typedef struct atmngr_subscribe_req* atmngr_subscribe_req_ptr_t;
typedef struct atmngr_act_node atmngr_act_node_t;
typedef struct atmngr_act_node* atmngr_act_node_ptr_t;
typedef struct atmngr_act_tbl_entry atmngr_act_tbl_entry_t;
typedef struct atmngr_act_tbl_entry* atmngr_act_tbl_entry_ptr_t;
typedef void (*atmngr_act_fp_t) (const char *atc, void *arg);
#endif

// ==== Legacy ====
#define ATI_ATC_PREFIX_MAX_LEN	(32)	
// ==== Legacy ====
#if 0
#define ATMNGR_PREFIX_MAX_LEN	(32)
#endif

// Variables
extern int mal_mode;   // NEED_TO_BE_NOTICED, where to put?
// => ATI task
extern int ati_sock;
extern int ati_urc_sock;
// => ATI entity
// ==== Legacy ====
struct ati_reg_tbl_entry
{
    char atc_prefix[ATI_ATC_PREFIX_MAX_LEN];
    event_id_t event_id;
    ati_atc_hdl_t hdl;
    chain_t mailbox_list;
};

struct ati_reg_req
{
    task_id_t task_id;
    event_id_t event_id;
	char atc_prefix[ATI_ATC_PREFIX_MAX_LEN];
};
// ==== Legacy ====

#if 0
#define ATMNGR_URC_HDR  ("\r\n")
#define ATMNGR_URC_TRL  ("\r\n")

struct atmngr_subscribe_req
{
	char prefix[ATMNGR_PREFIX_MAX_LEN];
    context_id_t context_id;
    event_id_t event_id;
    bool does_intercept;
};

struct atmngr_act_tbl_entry
{
    char prefix[ATMNGR_PREFIX_MAX_LEN]; // NEED_TO_BE_NOTICED, NOT necessary
    bool does_intercept;
    size_t intercept_cnt;  // NEED_TO_BE_NOTICED, a variable-length variable
    chain_t act_list;
};

struct atmngr_act_node
{
    context_id_t context_id;
    event_id_t event_id;
    bool does_intercept;
    atmngr_act_fp_t act_fp;
};
#endif

#endif
